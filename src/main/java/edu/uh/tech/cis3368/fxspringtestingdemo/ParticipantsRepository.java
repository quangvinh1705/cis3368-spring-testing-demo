package edu.uh.tech.cis3368.fxspringtestingdemo;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ParticipantsRepository extends CrudRepository<Participants,Integer> {

    List<Participants> findByWinnerFalse();
    Participants findByLastNameIs(String lastname);
}
