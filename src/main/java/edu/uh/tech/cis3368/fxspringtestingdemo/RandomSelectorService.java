package edu.uh.tech.cis3368.fxspringtestingdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.parser.Part;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RandomSelectorService {

    ParticipantsRepository repository;

    @Autowired
    public RandomSelectorService(ParticipantsRepository repository) {
        System.out.println("in the Selector service ctor");
        this.repository = repository;
    }

    public List<Participants> findLosers(){
        return repository.findByWinnerFalse();
    }

    public Participants nextWinner(){
        Participants winner;

        List<Participants> potentialWinners = repository.findByWinnerFalse();
        // pick one at random...

        return potentialWinners.get(1);
    }
}
