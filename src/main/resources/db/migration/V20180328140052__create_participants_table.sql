CREATE TABLE participants
(
  id int  PRIMARY KEY AUTO_INCREMENT,
  last_name varchar(24),
  winner boolean DEFAULT false
);